
app.factory('Branches', function($resource) {
	return $resource('/rest/entrypoint/branches', {});
})

.factory('EntryPoints', function($resource) {
	return $resource('/rest/entrypoint/branches/:branchId/entryPoints/deviceTypes/SW_RECEPTION', {});
})

.factory('User2', function($resource) {
	return $resource('/rest/entrypoint/user', {});
})
.factory('User', function($resource) {
	return $resource('/rest/managementinformation/v2/branches/:branchId/users/:userName', {});
})


.factory('Services', function($resource) {
	return $resource('/rest/entrypoint/branches/:branchId/services', {});
})

.factory('Queues', function($resource) {
	return $resource('/rest/entrypoint/branches/:branchId/queues', {});
})

.factory('Visits', function($resource) {
	return $resource('/rest/entrypoint/branches/:branchId/queues/:queueId/visits', {});
})

.factory('Visit', function($resource) {
	return $resource('/rest/entrypoint/branches/:branchId/entryPoints/:entryPointId/visits/',null, {'update': {method: 'PUT', headers:{'Content-Type':'application/json'},isArray:true}});
})

.factory('UpdateVisit', function($resource) {
	return $resource('/rest/servicepoint/branches/:branchId/queues/:queueId/visits/',null, {'update': {method: 'PUT',headers:{'Content-Type':'application/json'},isArray:true}});
})
.factory('TinyVisit', function($resource) {
	return $resource('/rest/entrypoint/branches/:branchId/entryPoints/:entryPointId/visits/:ticketId', {});
})
.factory('ServicePoints', function($resource) {
	return $resource('/rest/managementinformation/branches/:branchId/servicePoints', {});
});
// /rest/managementinformation/v2/branches/1/users/superadmin/