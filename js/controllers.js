
app.controller('mainController', function($scope, $modal, $state, User,User2) {
	$scope.user = {};
	$scope.user2 = {};
	
	User2.get(function(data) {
		$scope.user = data;
	});
	
	
	
	if (typeof sessvars.branchId === 'undefined' || sessvars.branchId == null || sessvars.branchId == -1 ||
      typeof sessvars.entryPointId === 'undefined' || sessvars.entryPointId == null || sessvars.entryPointId == -1) {
	  var modalInstance = $modal.open({
			backdrop: 'static',
			keyboard: false,
	    templateUrl: 'settings.html',
	    controller: settingsController
	  });
		
	  modalInstance.result.then(function(settings) {
			sessvars.branchId = settings.branch;
			sessvars.entryPointId = settings.entryPoint;
		
			$state.go('/ep');
	  });
	} else {
		$state.go('/ep');
	}
	
	$scope.showSettings = function () {
		var settingsInstance =  $modal.open({
			backdrop: 'static',
			keyboard: false,
	    templateUrl: 'settings.html',
	    controller: settingsController
	  });
		
		settingsInstance.result.then(function(settings) {
			sessvars.branchId = settings.branch;
			sessvars.entryPointId = settings.entryPoint;
		
			$state.go('/ep');
			window.location.reload(false);
	  });
	 
	};
});

app.controller('epController', function($scope, $timeout, Services, Queues, Visits, Visit, ServicePoints, TinyVisit,$modal , UpdateVisit,User) {
	$scope.toggleOpen = false;
	$scope.servicepoints = [];
	$scope.services = [];
	$scope.queues = [];
	$scope.queueId = 0;
	$scope.visits = [];
	$scope.tinyvisit = [];
	$scope.visit = {
		services: [],
		parameters: {
			customerName: "",
			mobile: ""
		}
	};
	$scope.alerts = [];
	
	User.query({branchId: sessvars.branchId}, function(data) {
		$scope.user2 = data;
	},function(error){
		$scope.user2 = error.status;
		
		if(error.status == '401')
		{
		alert("You are not allowed to view this page.");
			history.back();
		}
	});
	
	
	Services.query({ branchId: sessvars.branchId }, function(data) {
			$scope.services = data;
	});
	var customersWaiting = 0;
	var maxwaittime = 0;
	var averageCustomersWaiting = 0;
	var servingCustomers = 0;
	var openCounters = 0;
		
	(function getQueues() {
		customersWaiting = 0;
		maxwaittime = 0;
		Queues.query({ branchId: sessvars.branchId }, function(data) {
			
			for(i in data) {
				if(data[i].id == $scope.queueId) {
					data[i].open = true;
					$scope.getVisits();
				} else {
					data[i].open = false;
				}
				if(typeof data[i].customersWaiting === "undefined")
				{
					
				}
				else
				{
				 customersWaiting = customersWaiting + data[i].customersWaiting;
				}
				if(typeof data[i].waitingTime === "undefined")
				{
					
				}
				else
				{
					if(data[i].waitingTime > maxwaittime)
					{
					maxwaittime = data[i].waitingTime;
					}
				}
			}
			console.log("MAx Customers waitingTime : " + maxwaittime );
			if(customersWaiting != 0)
			averageCustomersWaiting = maxwaittime / customersWaiting;
			
			console.log("Average Customers waitingTime : " + averageCustomersWaiting );
			document.getElementById("bCustomersWaiting").innerHTML  = customersWaiting;
			document.getElementById("bMaxWaiting").innerHTML  = $scope.formatTime( maxwaittime);
			document.getElementById("bAvgWaiting").innerHTML  = $scope.formatTime(averageCustomersWaiting);
			$scope.queues = data;			
			$timeout(getQueues, 10*1000);
		});
		openCounters = 0;
		servingCustomers = 0;
		ServicePoints.query({ branchId: sessvars.branchId }, function(data) {
			//console.log("Data for ServicePoints : " + data);
			$scope.servicepoints = data;
			for(i in data) {
			if(typeof data[i].status === "undefined" || data[i].status == 'CLOSED')
				{
					//console.log("Branch : " + data[i].name + " is " + data[i].status);
				}
				else if(data[i].status == 'OPEN')
				{
					//console.log("Branch : " + data[i].name + " is " + data[i].status);
				 openCounters++;
				}
			
			if(typeof data[i].currentTicketNumber === "undefined" || data[i].currentTicketNumber == "" )
				{
					//console.log("Branch : " + data[i].name + " is " + data[i].status);
				}
				else 
				{
					//console.log("Branch : " + data[i].name + " is " + data[i].status);
				 servingCustomers++;
				}
			
			}
			//console.log("Total open Counters : " + openCounters);
				document.getElementById("bServingCustomers").innerHTML  = servingCustomers;
			document.getElementById("bOpenServicePoints").innerHTML = openCounters;
		});
		
		
			
	})();
	$scope.showOptions = function (id,fromid) {
		sessvars.ticketId = id;
		sessvars.fromQueue = fromid;
		var settingsInstance =  $modal.open({
			backdrop: 'static',
			keyboard: false,
	    templateUrl: 'options.html',
	    controller: optionsController
	  });
		
		settingsInstance.result.then(function(settings) {
			//sessvars.toqueueId = settings.toqueueId;
			//console.log("Queue to forward : " + sessvars.toqueueId);
		//Call ticket move
		
	  });
	 
	};
	$scope.showVisits = function(id) {
		servingCustomers = 0;
		if($scope.queueId == id) {
			$scope.queueId = 0;
			
			for(i in $scope.queues) {
				if($scope.queues[i].id == id)
					$scope.queues[i].open = false;
				
			}
			
			return;
		}
		for(i in $scope.queues) {
			if($scope.queues[i].id == id)
				$scope.queues[i].open = true;
			servingCustomers++;
		}
		$scope.queueId = id;
		
		$scope.getVisits();
	};
	
	$scope.getVisits = function(id) {
		servingCustomers = 0;
		Visits.query({ branchId: sessvars.branchId, queueId: $scope.queueId }, function(data) {
			$scope.visits = data;
			
			for(i in $scope.queues) {
			if($scope.queues[i].id == id)
				servingCustomers++;
		}
			
		});
		
		document.getElementById("bServingCustomers").innerHTML  = servingCustomers;
	};
	
	$scope.deleteVisit = function(id) {
		
		TinyVisit.delete({ branchId: sessvars.branchId, entryPointId: sessvars.entryPointId,ticketId: id },function(data) {
	
			$scope.alerts.push({type: "success", msg: "Ticket Deleted"});
			$timeout(function(){
				$scope.alerts.splice(0, 1);
			}, 5*1000);
		});
	$scope.getVisits();
	};
	$scope.createVisit = function() {
		Visit.save({ branchId: sessvars.branchId, entryPointId: sessvars.entryPointId }, $scope.visit, function(data) {
			$scope.visit = {
				services: [],
				parameters: {
					customerName: "",
					mobile: ""
				}
			};
			
			$scope.alerts.push({type: "success", msg: "Ticket number " + data.ticketId});
			$timeout(function(){
				$scope.alerts.splice(0, 1);
			}, 5*1000);
		});
	};
	
	$scope.formatTime = function(secsIn) {
	    if(secsIn == -1) {
	        return "";
	    }
	    var hours = parseInt(secsIn / 3600);
	    var remainder = secsIn % 3600;
	    var minutes = parseInt(remainder / 60);
	    var formatted = (hours < 10 ? "0" : "") + hours
	            + ":" + (minutes < 10 ? "0" : "") + minutes;
	    return formatted;
	};
	
});

var optionsController = function ($scope, $timeout,$modalInstance,EntryPoints,Queues ,TinyVisit ,Visit , UpdateVisit )
{
	//sessvars.fromQueue = fromid;
	$scope.queuestest = [];
	$scope.alerts = [];
	Queues.query({ branchId: sessvars.branchId }, function(data) {
	console.log("Queue Data : " + data);
	$scope.queues = data;
	});
	$scope.selectBranch = "";
	$scope.selectQueue = function() {
		
		var e = document.getElementById("QueueToTransfer");
		var value = e.options[e.selectedIndex].text;
		var value2 = e.options[e.selectedIndex].value;
		console.log("Selected Queue " + value  + " Testing " + value2);
		$scope.selectBranch = value2;
		
	};
	// $scope.selectBranch = function() {
		// var testCount = 0;
			// EntryPoints.query({ branchId: $scope.settings.branch }, function(data) {
			// $scope.entrypoints = data;
			
		// });
	// };
	if (typeof sessvars.toqueueId != 'undefined' || sessvars.toqueueId != null || sessvars.toqueueId > 0)
		$scope.selectQueue();
	
	
	$scope.closeOption = function() {
		$modalInstance.close();
	};
	$scope.deleteOption = function(id) {
		console.log("Ticket to delete " + sessvars.ticketId);
		TinyVisit.delete({ branchId: sessvars.branchId, entryPointId: sessvars.entryPointId,ticketId: sessvars.ticketId  },function(data) {
	
			
		});
		$scope.alerts.push({type: "success", msg: "Ticket Deleted"});
			$timeout(function(){
				$scope.alerts.splice(0, 1);
			}, 5*1000);
			$timeout(function(){
				$modalInstance.close();
			}, 1*1000);
	};
	$scope.transferOptionFirst = function() {
		
					
					
		UpdateVisit.update({ branchId: sessvars.branchId, queueId: $scope.selectBranch },
			{
			  "fromId":sessvars.fromQueue,"fromBranchId":sessvars.branchId,"visitId":sessvars.ticketId,"sortPolicy":"FIRST"
					}, function(data) {
					//alert(data.status);
			}
			);
			
			$scope.alerts.push({type: "success", msg: "Ticket was Transfered "});
			$timeout(function(){
				$scope.alerts.splice(0, 1);
				$modalInstance.close();
			}, 5*1000);
		
	};
	$scope.transferOptionLast = function() {
		
					
					
		UpdateVisit.update({ branchId: sessvars.branchId, queueId: $scope.selectBranch  },
			{
			  "fromId":sessvars.fromQueue,"fromBranchId":sessvars.branchId,"visitId":sessvars.ticketId,"sortPolicy":"LAST"
					}, function(data) {
					//alert(data.status);
			}
			);
			
			$scope.alerts.push({type: "success", msg: "Ticket was Transfered "});
			$timeout(function(){
				$scope.alerts.splice(0, 1);
				$modalInstance.close();
			}, 5*1000);
		
	};
	$scope.transferOptionSorted = function() {
		
					
					
		UpdateVisit.update({ branchId: sessvars.branchId, queueId: $scope.selectBranch },
			{
			  "fromId":sessvars.fromQueue,"fromBranchId":sessvars.branchId,"visitId":sessvars.ticketId,"sortPolicy":"SORTED"
					}, function(data) {
					//alert(data.status);
			}
			);
			
			$scope.alerts.push({type: "success", msg: "Ticket was Transfered "});
			$timeout(function(){
				$scope.alerts.splice(0, 1);
				$modalInstance.close();
			}, 5*1000);
		
	};
}



var settingsController = function ($scope, $modalInstance, Branches, EntryPoints) {
	$scope.branches = [];
	$scope.entrypoints = [];
	$scope.settings = {
		branch: sessvars.branchId,
		entryPoint: sessvars.entryPointId
	};
	
	Branches.query(function(data) {
		$scope.branches = data;
	});
	
	$scope.selectBranch = function() {
		var testCount = 0;
			EntryPoints.query({ branchId: $scope.settings.branch }, function(data) {
			$scope.entrypoints = data;
			
		});
	};
	
	if (typeof sessvars.branchId != 'undefined' || sessvars.branchId != null || sessvars.branchId > 0)
		$scope.selectBranch();
	
	$scope.settingsOK = function() {
		if($scope.settings.branch != undefined && $scope.settings.entryPoint != undefined) {
			return true;
		}
		
		return true;
	}
	
	$scope.ok = function() {
		$modalInstance.close($scope.settings);
	};
	$scope.notOk = function() {
		$modalInstance.close();
	};
	
};